import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom'

import PostScreen from './screens/Post'
import LoginScreen from './screens/Login'
import UserLoged from './utils/UserLoged'
import SearchScreen from './screens/Search'
import NewPostScreen from './screens/NewPost'
import LandingScreen from './screens/Landing'
import RegisterScreen from './screens/Register'

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<LandingScreen/>} />
        <Route path='/login' element={!UserLoged ? <LoginScreen/> : <Navigate to='/'/>} />
        <Route path='/register' element={!UserLoged ? <RegisterScreen/> : <Navigate to='/'/>} />
        <Route path='/create-post' element={UserLoged ? <NewPostScreen/> : <Navigate to='/'/>} />
        <Route path='/post/:id/*' element={<PostScreen/>} />
        <Route path='/search' element={<LandingScreen/>} />
        <Route path='/search/:searchValue' element={<SearchScreen/>} />
      </Routes>
    </BrowserRouter>
  )
}

export default App
