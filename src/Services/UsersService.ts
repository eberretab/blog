import { User } from "../types";

const API = import.meta.env.VITE_API_URL;

class UsersService {
  async login(email:string, password:string) : Promise<User>{
    const response = await fetch(`${API}/auth/login`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({email, password})
    });
    const data = await response.json();
    return data;
  }

  async register(name: string, email:string, password:string) : Promise<User>{
    const response = await fetch(`${API}/auth/register`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({name, email, password})
    });
    const data = await response.json();
    return data;
  }
}   

export default new UsersService();