import { Post } from "../types";
import UserLoged from "../utils/UserLoged";

const API = import.meta.env.VITE_API_URL;

class PostsService {
  async getAllPosts() : Promise<(Post & {author_name: string})[]>{
    const response = await fetch(`${API}/posts`);
    const data = await response.json();
    return data;
  }

  async getPost(postId:string){
    const response = await fetch(`${API}/posts/${postId}`);
    const data = await response.json();
    return data;
  }

  async createPost(title:string, content:string){
    const response = await fetch(`${API}/posts`, {
      method: 'POST',
      headers: { 
        'Content-Type': 'application/json',
        'authorization': `Bearer ${UserLoged.access_token}`
       },
      body: JSON.stringify({title, content})
    });
    const data = await response.json();
    return data;
  }

  async searchPosts(search:string){ 
    const response = await fetch(`${API}/posts/search/${search}`);
    const data = await response.json();
    return data;
  }
}   

export default new PostsService();