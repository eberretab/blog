import { User } from "../types";

const user = localStorage.getItem('user')

export default (user ? JSON.parse(user || '') : null ) as User