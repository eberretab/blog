export type Post = {
    id: number;
    author: User;
    title: string;
    content: string;
    created_at: string;
};

export type User = {
    id: number;
    name: string;
    access_token: string;
    email: string;
};