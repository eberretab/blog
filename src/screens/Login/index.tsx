/** @jsxImportSource @emotion/react */
import * as Yup from 'yup';
import { useState } from 'react';
import { css } from '@emotion/react';
import { Field, Form, Formik } from 'formik';

import Header from '../../components/Header';
import UsersService from '../../Services/UsersService';

const styles = css`
  color: white;
  margin: 0 auto;
  max-width: 400px;
  padding-top: 2em;
  
  
  h1 {
    text-align: center;
    font-size: 2rem;
  }
}
`
const LoginValidateSchema = Yup.object({
  email: Yup.string().email('Correo electrónico invalido').required('Correo electrónico requerido'),
  password: Yup.string().required('Contraseña requerida')
});

type LoginValues = {
  email: string;
  password: string;
}

export default function LoginScreen() {
  const [error, setError] = useState('');

  const submitLogin = (values: LoginValues) => {
    setError('');
    UsersService.login(values.email, values.password).then((data) => {
      console.log(data);
      if(!data.id) {
        setError('Correo electrónico o contraseña incorrectos');
        return;
      }else{
        localStorage.setItem('user', JSON.stringify(data));
        location.href = '/';
      }
    });
  }

  return (
    <>
      <Header />
      <div css={styles}>
        <div className="card p-4 pb-5">
          <h1 className='mt-2'>Iniciar sesión</h1>

          <p className='text-center mb-4 '>Inicia sesión para poder publicar en el blog</p>

          <Formik
            initialValues={{ email: '', password: '' }}
            onSubmit={submitLogin}
            validationSchema={LoginValidateSchema}
          >
            {() => {
              return (
                <Form>
                  <div className='mb-4'>
                    <p>Correo electrónico</p>
                    <Field type="email" className='form-control' name="email" />
                  </div>

                  <div className='mb-4'>
                    <p>Contraseña</p>
                    <Field type="password" className='form-control' name="password" />
                  </div>

                  <div className='text-center'>
                    <button type='submit' className='btn btn-success w-100'>Iniciar sesión</button>
                  </div>
                  {error && <p className='text-danger text-center mt-2'>{error}</p>}

                </Form>)
            }}
          </Formik>
        </div>
      </div>
    </>
  )
}
