/** @jsxImportSource @emotion/react */
import * as Yup from 'yup';
import { useState } from 'react';
import { css } from '@emotion/react';
import { Field, Form, Formik } from 'formik';

import Header from '../../components/Header';
import UsersService from '../../Services/UsersService';

const styles = css`
  color: white;
  margin: 0 auto;
  max-width: 400px;
  padding-top: 2em;
  
  
  h1 {
    text-align: center;
    font-size: 2rem;
  }
}
`
const RegisterValidateSchema = Yup.object({
  email: Yup.string().email('Correo electrónico invalido').required('Correo electrónico requerido'),
  password: Yup.string().required('Contraseña requerida'),
  name: Yup.string().required('Nombre requerido')
});

type RegisterValues = {
  email: string;
  password: string;
  name: string;
}

export default function RegisterScreen() {
    const [error, setError] = useState('');

    const submitLogin = (values: RegisterValues) => {
      setError('');
      UsersService.register(values.name, values.email, values.password).then((data) => {
        if(!data.id) {
          setError('No se pudo crear el usuario con los datos proporcionados');
          return;
        }else{
          localStorage.setItem('user', JSON.stringify(data));
          alert("Usuario creado correctamente")
          location.href = '/';
        }
      });
    }
  
    return (
      <>
        <Header />
        <div css={styles}>
          <div className="card p-4 pb-5">
            <h1 className='mt-2'>Crear cuenta</h1>
  
            <p className='text-center mb-4'>Crea tu cuenta y realiza tus publicaciones en blog</p>
  
            <Formik
              onSubmit={submitLogin}
              initialValues={{ name:'', email: '', password: '' }}
              validationSchema={RegisterValidateSchema}
            >
              {() => {
                return (
                  <Form>
                    <div className='mb-4'>
                      <p>Nombre</p>
                      <Field type="text" className='form-control' name="name" />
                    </div>

                    <div className='mb-4'>
                      <p>Correo electrónico</p>
                      <Field type="email" className='form-control' name="email" />
                    </div>
  

                    <div className='mb-4'>
                      <p>Contraseña</p>
                      <Field type="password" className='form-control' name="password" />
                    </div>
  
                    <div className='text-center'>
                      <button type='submit' className='btn btn-success w-100'>Registrarme</button>
                    </div>
                    {error && <p className='text-danger text-center mt-2'>{error}</p>}
  
                  </Form>)
              }}
            </Formik>
          </div>
        </div>
      </>
    )
  }
  