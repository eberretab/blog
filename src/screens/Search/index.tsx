/** @jsxImportSource @emotion/react */
import { css } from '@emotion/react';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import { Post } from '../../types';
import Header from '../../components/Header';
import PostsList from '../../components/PostsList';
import PostsService from '../../Services/PostsService';

const styles = css`
  padding: 2rem;
  color: white;
  text-align: center;
  
  .title-container{
    margin-top: 2rem;
    margin-bottom: 3rem;
  }
  .posts-container{
    max-width: 1200px;
    margin: 0 auto;
  }
`

export default function SearchScreen() {
  const {searchValue} = useParams();

  const [posts, setPosts] = useState<(Post & { author_name: string })[]>([])

  useEffect(() => {
    PostsService.searchPosts(searchValue || '').then(data => {
      setPosts(data);
    })

  }, [searchValue])

  return (
    <>
    <Header/>
      <div css={styles}>
        <div className='title-container'>
            <p>{posts.length} resultado{posts.length === 1 ? '' :'s' } para "{searchValue}"</p>
        </div>

        <div className='posts-container'>
          <PostsList posts={posts} />
        </div>
      </div>
    </>
  )
}
