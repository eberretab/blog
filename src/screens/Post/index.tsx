/** @jsxImportSource @emotion/react */
import moment from 'moment';
import { css } from '@emotion/react';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import { Post } from '../../types';
import Header from '../../components/Header';
import PostsService from '../../Services/PostsService';

const styles = css`
    padding: 2rem;
    color: white;
    text-align:center;
    max-width: 950px;
    margin: 0 auto;
    flex-direction: column;
    display: flex;
    gap: 3rem;

    .title{
        margin-top: 1rem;
    }

    .card {
     white-space: pre-line;
     }
`

export default function PostScreen() {
    const { id } = useParams();
    const [post, setPost] = useState<Post>()

    useEffect(() => {
        if (id === undefined) return
        PostsService.getPost(id).then((data) => {
            setPost(data)
        })
    }, [id])
    return (
        <>
            <Header />
            <div css={styles}>
                <h1 className='title'>{post?.title}</h1>
                <div className='card p-4' >
                    <p>{post?.content}</p>
                </div>
                <div className='d-flex justify-content-between'>
                    <p>Escrito por <b>{post?.author?.name}</b></p>
                    <p>{moment(post?.created_at).fromNow()}</p>
                </div>
            </div>
        </>
    )
}
