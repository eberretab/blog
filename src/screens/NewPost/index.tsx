
/** @jsxImportSource @emotion/react */
import * as Yup from 'yup';
import { useState } from 'react';
import { css } from '@emotion/react';
import { Field, Form, Formik } from 'formik';
import { useNavigate } from 'react-router-dom';

import Header from '../../components/Header';
import PostsService from '../../Services/PostsService';

const styles = css`
  color: white;
  margin: 0 auto;
  max-width: 400px;
  padding-top: 2em;
  
  h1 {
    text-align: center;
    font-size: 2rem;
  }
}
`
const PostValidateSchema = Yup.object({
  title: Yup.string().required('El título es requerido'),
  content: Yup.string().required('El contenido es requerido')
});

type PostValues = {
  title: string;
  content: string;
}

export default function NewPostScreen() {
  const [error, setError] = useState<string>('');
  const navigate = useNavigate();

  const submitPost = (values: PostValues) => {
    setError("");
    PostsService.createPost(values.title, values.content).then((data) => {
      if (data.id) {
        alert('Publicación creada')
        navigate('/');
      } else {
        setError('Error al crear la publicación');
      }
    });

  }
  return (
    <>
      <Header/>
      <div css={styles}>
        <div className="card p-4 pb-5">
          <h1 className='mt-2 mb-4'>Nueva publicación</h1>

          <small className='text-center mb-3 '>Redacta tu publicación y presiona <b>Enviar</b> para subirla</small>

          <Formik
            initialValues={{ title: '', content: '' }}
            onSubmit={submitPost}
            validationSchema={PostValidateSchema}
          >
            {({values, errors}) => {
              return (
                <Form>
                  <div className='mb-4'>
                    <p>Título</p>
                    <Field type="text" className='form-control form-control-sm' name="title" placeholder="Ingresa aquí el título" />
                    {errors.title && <small className='text-danger'>{errors.title}</small>}
                  </div>

                    <p>Contenido</p>
                  <div className='mb-4'>
                    <Field component='textarea' rows={8} className='form-control form-control-sm' name="content" placeholder="Ingresa aqui el contenido..." />
                    <small>{values.content.length} caracteres</small>
                    {errors.content && <small className='text-danger d-block'>{errors.content}</small>}
                  </div>

                  <div className='text-center'>
                    <button type='submit' className='btn btn-success w-100'>Enviar</button>
                  </div>
                  {error && <p className='text-danger text-center mt-2'>{error}</p>}
                </Form>)
            }}
          </Formik>
        </div>
      </div>
    </>
  )
}
