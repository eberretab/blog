/** @jsxImportSource @emotion/react */
import 'moment/locale/es';
import moment from 'moment'
import { css } from '@emotion/react'
import { Link } from 'react-router-dom';

import { Post } from '../types'

const style = css`
    h2 {
        font-size: 1.25rem;
    }
`
moment.locale('es');    

export type PostItem = { author_name: string } & Post;

export default function PostsList({ posts }: { posts: PostItem[] }) {
    return (
        <div className='row' css={style}>
            {
                posts.map(post => {
                    return (
                        <div key={post.id} className='col-md-6 mb-4'>
                            <div className='card p-4 pb-1'>
                                <Link to={`/post/${post.id}/${post.title.toLowerCase().replaceAll(' ', '-')}`}>
                                    <h2>{post.title}</h2>
                                </Link>
                                <p>{post.content}</p>
                                <div className='d-flex justify-content-between mx-2'>
                                    <p>{post.author_name}</p>
                                    <p>{moment(post.created_at).lang('es').fromNow()}</p>
                                </div>
                            </div>
                        </div>
                    )
                })
            }
            {
                posts.length === 0 && <p>No hay publicaciones</p>
            }
        </div>
    )
}
