/** @jsxImportSource @emotion/react */
import { css } from '@emotion/react'
import { Link } from 'react-router-dom'

import SearchInput from './SearchInput'
import UserLoged from '../utils/UserLoged'

const style = css`
  padding: 0 20px;
  background-color: #333;
  
  a {
    color: white;
  }

  a:hover, cursor-pointer:hover {
    color: #ccc;
  }

  .cursor-pointer {
    cursor: pointer;
  }

  .input-group{
    width: 400px;
  }
`

export default function Header() {
  const removeSession = () => {
    localStorage.removeItem('user');
    window.location.reload();
  }

  return (
    <div css={style} className='row w-100 mx-0 py-3'>
      <div className='col-lg-4 d-flex align-items-center'>
        <Link to={'/'}>Posts</Link>
      </div>

      <div className='col-lg-4 d-flex align-items-center justify-content-center'>
        <SearchInput />
      </div>

      <div className='col-lg-4 d-flex justify-content-end align-items-center'>
        {
          UserLoged ?
            <div className='d-flex'>
              <Link
                className='mx-3 text-white' to={'/create-post'}>Crear Publicación</Link>
              <p className='mx-3 text-ligth cursor-pointer' onClick={removeSession}>Cerrar Sesión</p>
            </div>
            : 
            <div className='d-flex'>
              <Link className='mx-3 text-white' to={'/login'}>Iniciar Sesión</Link>
              <Link className='mx-3 text-white' to={'/register'}>Crear Cuenta</Link>
            </div>
        }
      </div>
    </div>
  )
}
