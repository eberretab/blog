import { useState } from "react"
import { useNavigate, useParams } from "react-router-dom"

export default function SearchInput() {
    const navigate = useNavigate()
    const params = useParams();
    
    const [searchValue, setSearchValue] = useState(params.searchValue || '' )

    const handleSearch = () => {
        navigate(`/search/${searchValue}`)
     }

    return (
        <div className="input-group w-100">
            <input
                type="text"
                value={searchValue}
                placeholder="Buscar..."
                className="form-control form-control-sm"
                onChange={(e) => setSearchValue(e.target.value)}
                onKeyUp={(e) => e.key === 'Enter' && handleSearch()}
            />
            <div className="input-group-append">
                <button
                    type="button"
                    onClick={handleSearch}
                    className="btn btn-secondary btn-sm"
                >
                    Buscar
                </button>
            </div>
        </div>
    )
}
