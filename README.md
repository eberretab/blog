# React + TypeScript + Vite Blog App

This is a Blog App created with React, TypeScript and Vite.

# Steps to run
1. Install yarn dependencies 

   `yarn install`

2. Copy .env.example as .env and set `VITE_API_URL` to set API host url. 

   ``cp .env.example .env``

3. Run project 

   `yarn dev`

#### Author: Eber Reta